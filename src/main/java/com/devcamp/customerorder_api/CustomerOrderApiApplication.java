package com.devcamp.customerorder_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerOrderApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerOrderApiApplication.class, args);
	}

}
