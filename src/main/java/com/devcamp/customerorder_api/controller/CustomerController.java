package com.devcamp.customerorder_api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerorder_api.model.Customer;
import com.devcamp.customerorder_api.model.Order;
import com.devcamp.customerorder_api.repository.ICustomerRespository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CustomerController {
    @Autowired
    ICustomerRespository iCustomerRespository;

    @GetMapping("/devcamp-customers")
    public ResponseEntity<List<Customer>> getCustomerList(){
        try {
            List<Customer> lCustomers = new ArrayList<>();
            iCustomerRespository.findAll().forEach(lCustomers :: add);
            return new ResponseEntity<List<Customer>>(lCustomers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-orders")
    public ResponseEntity<Set<Order>> getOrderByUserId(@RequestParam(value = "userId",  required = true) long userId){
        try {
            Customer customer = iCustomerRespository.findByUserId(userId);

            if(customer != null){
                return new ResponseEntity<>(customer.getOrders(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
