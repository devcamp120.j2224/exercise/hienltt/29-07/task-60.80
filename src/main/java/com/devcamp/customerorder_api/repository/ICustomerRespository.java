package com.devcamp.customerorder_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerorder_api.model.Customer;

public interface ICustomerRespository extends JpaRepository<Customer, Long>{
    Customer findByUserId(long userId);
}
