package com.devcamp.customerorder_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerorder_api.model.Order;

public interface IOrderRespository extends JpaRepository<Order, Long>{
    
}
